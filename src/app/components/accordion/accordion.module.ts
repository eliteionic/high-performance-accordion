import { NgModule } from '@angular/core';
import { IonicModule } from '@ionic/angular';
import { AccordionGroupComponent } from './accordion-group/accordion-group.component';
import { AccordionItemComponent } from './accordion-item/accordion-item.component';

@NgModule({
  imports: [IonicModule],
  declarations: [AccordionGroupComponent, AccordionItemComponent],
  exports: [AccordionGroupComponent, AccordionItemComponent],
})
export class AccordionModule {}
