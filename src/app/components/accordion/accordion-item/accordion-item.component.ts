import {
  Component,
  OnInit,
  ViewChild,
  EventEmitter,
  Output,
  ElementRef,
  HostListener,
} from '@angular/core';

import { ToggleDetail } from '../interfaces/toggle-detail';

@Component({
  selector: 'app-accordion-item',
  templateUrl: './accordion-item.component.html',
  styleUrls: ['./accordion-item.component.scss'],
})
export class AccordionItemComponent implements OnInit {
  @Output()
  toggle: EventEmitter<ToggleDetail> = new EventEmitter<ToggleDetail>();

  @ViewChild('content', { read: ElementRef }) content: ElementRef;

  public isOpen = false;
  private isTransitioning = false;

  constructor(private element: ElementRef) {}

  @HostListener('click')
  toggleOpen() {
    if (this.isTransitioning) {
      return;
    }
    this.isOpen = !this.isOpen;
    this.isTransitioning = true;

    this.toggle.emit({
      element: this.element.nativeElement,
      content: this.content.nativeElement,
      shouldOpen: this.isOpen,
      startTransition: () => {
        this.isTransitioning = true;
      },
      endTransition: () => {
        this.isTransitioning = false;
      },
      setClosed: () => {
        this.isOpen = false;
      },
    });
  }

  ngOnInit() {}
}
