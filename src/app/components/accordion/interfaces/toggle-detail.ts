export interface ToggleDetail {
  element: HTMLElement;
  content: HTMLElement;
  shouldOpen: boolean;
  startTransition: () => void;
  endTransition: () => void;
  setClosed: () => void;
}
