import {
  Component,
  ElementRef,
  AfterViewInit,
  OnDestroy,
  ViewChild,
  ContentChildren,
  QueryList,
  Renderer2,
} from '@angular/core';
import { AnimationController, Animation } from '@ionic/angular';
import { Subject, merge } from 'rxjs';
import { concatMap, takeUntil } from 'rxjs/operators';

import { AccordionItemComponent } from '../accordion-item/accordion-item.component';
import { ToggleDetail } from '../interfaces/toggle-detail';

@Component({
  selector: 'app-accordion-group',
  templateUrl: './accordion-group.component.html',
  styleUrls: ['./accordion-group.component.scss'],
})
export class AccordionGroupComponent implements AfterViewInit, OnDestroy {
  @ViewChild('blocker', { read: ElementRef }) blocker: ElementRef;
  @ContentChildren(AccordionItemComponent)
  items: QueryList<AccordionItemComponent>;

  @ContentChildren(AccordionItemComponent, { read: ElementRef })
  itemElements: QueryList<ElementRef>;

  public shiftDownAnimation: Animation;
  public blockerDownAnimation: Animation;

  private elementsToShift: HTMLElement[];
  private currentlyOpen: ToggleDetail = null;
  private destroy$: Subject<boolean> = new Subject<boolean>();

  constructor(
    private renderer: Renderer2,
    private animationCtrl: AnimationController
  ) {}

  ngAfterViewInit() {
    const toggleEvents = [];

    this.items.forEach((item) => {
      toggleEvents.push(item.toggle);
    });

    merge(...toggleEvents)
      .pipe(
        concatMap((ev: ToggleDetail) =>
          ev.shouldOpen ? this.animateOpen(ev) : this.animateClose(ev)
        ),
        takeUntil(this.destroy$)
      )
      .subscribe((ev) => {
        ev.endTransition();
      });
  }

  ngOnDestroy() {
    this.destroy$.next(true);
    this.destroy$.unsubscribe();
  }

  async closeOpenItem() {
    if (this.currentlyOpen !== null) {
      const itemToClose = this.currentlyOpen;

      itemToClose.startTransition();
      await this.animateClose(this.currentlyOpen);
      itemToClose.endTransition();
      itemToClose.setClosed();
    }
  }

  async animateOpen(ev: ToggleDetail) {
    // Close any open item first
    await this.closeOpenItem();
    this.currentlyOpen = ev;

    // Find the item being opened, and create a new array with only the elements beneath the element being opened
    let splitOnIndex = 0;

    this.itemElements.forEach((item, index) => {
      if (item.nativeElement === ev.element) {
        splitOnIndex = index;
      }
    });

    this.elementsToShift = [...this.itemElements]
      .map((item) => item.nativeElement)
      .splice(splitOnIndex + 1, this.itemElements.length - (splitOnIndex + 1));

    // Set item content to be visible
    this.renderer.setStyle(ev.content, 'display', 'block');

    // Calculate the amount other items need to be shifted
    const amountToShift = ev.content.clientHeight;
    const openAnimationTime = 300;

    // Initially set all items below the one being opened to cover the new content
    // but then animate back to their normal position to reveal the content
    this.shiftDownAnimation = this.animationCtrl
      .create()
      .addElement(this.elementsToShift)
      .delay(20)
      .beforeStyles({
        ['transform']: `translateY(-${amountToShift}px)`,
        ['position']: 'relative',
        ['z-index']: '1',
      })
      .afterClearStyles(['position', 'z-index'])
      .to('transform', 'translateY(0)')
      .duration(openAnimationTime)
      .easing('cubic-bezier(0.32,0.72,0,1)');

    // This blocker element is placed after the last item in the accordion list
    // It will change its height to the height of the content being displayed so that
    // the content doesn't leak out the bottom of the list
    this.blockerDownAnimation = this.animationCtrl
      .create()
      .addElement(this.blocker.nativeElement)
      .delay(20)
      .beforeStyles({
        ['transform']: `translateY(-${amountToShift}px)`,
        ['height']: `${amountToShift}px`,
      })
      .to('transform', 'translateY(0)')
      .duration(openAnimationTime)
      .easing('cubic-bezier(0.32,0.72,0,1)');

    await Promise.all([
      this.shiftDownAnimation.play(),
      this.blockerDownAnimation.play(),
    ]);

    return ev;
  }

  async animateClose(ev: ToggleDetail) {
    this.currentlyOpen = null;
    const amountToShift = ev.content.clientHeight;

    const closeAnimationTime = 300;

    // Now we first animate up the elements beneath the content that was opened to cover it
    // and then we set the content back to display: none and remove the transform completely
    // With the content gone, there will be no noticeable position change when removing the transform
    const shiftUpAnimation: Animation = this.animationCtrl
      .create()
      .addElement(this.elementsToShift)
      .afterStyles({
        ['transform']: 'translateY(0)',
      })
      .to('transform', `translateY(-${amountToShift}px)`)
      .afterAddWrite(() => {
        this.shiftDownAnimation.destroy();
        this.blockerDownAnimation.destroy();
      })
      .duration(closeAnimationTime)
      .easing('cubic-bezier(0.32,0.72,0,1)');

    const blockerUpAnimation: Animation = this.animationCtrl
      .create()
      .addElement(this.blocker.nativeElement)
      .afterStyles({
        ['transform']: 'translateY(0)',
      })
      .to('transform', `translateY(-${amountToShift}px)`)
      .duration(closeAnimationTime)
      .easing('cubic-bezier(0.32,0.72,0,1)');

    await Promise.all([shiftUpAnimation.play(), blockerUpAnimation.play()]);

    // Hide the content again
    this.renderer.setStyle(ev.content, 'display', 'none');

    // Destroy the animations to reset the CSS values that they applied. This will remove the transforms instantly.
    shiftUpAnimation.destroy();
    blockerUpAnimation.destroy();

    return ev;
  }
}
